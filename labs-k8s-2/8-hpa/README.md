
# Lab - deploying and testing the horizontal pod autoscale resource

Deploy a hpa to scale a deployment based on CPU utilization 

ToDo

- Deploy `deployment.yaml`
  - Take a look at the the resource requests and limits
- Get the current CPU utilization of a pod with `kubectl top`
- Deploy the HPA
- Keep an eye on the hpa `kubectl get hpa hpa-deployment -w`
- Run the following command in a seperate terminal session
  ```
  kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://hpa-deployment; done"
  ```

- How long does it roughly take for the application to scale?
- How many replicas does it take to settle the hpa?
- If you set the `targetCPUUtilizationPercentage` to a different value, how many replicas will it scale to?
