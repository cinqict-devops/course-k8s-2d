# Lab - Look Around

ToDo

- Look around
- How many nodes?
  `kubectl get nodes`
- How many pods?
  `kubectl get pods -A`
- What do you see more?
- `kubectl cluster-info`

## Learning goals

- Get familiar with
  - `kubectl get pods/nodes`
  - default nodes, pods
